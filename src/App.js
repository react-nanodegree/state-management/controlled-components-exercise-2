import React from 'react';
import logo from './logo.svg';
import './App.css';

class ShoppingListForm extends React.Component {
  render(){
    return (
      <form onSubmit={this.props.onFormSubmit}>
      <input
        type="text"
        placeholder="Enter New Item"
        value={this.props.inputValue}
        onChange={this.props.onInputChange}
      />
      <button disabled={this.props.addButtonDisabled}>Add</button>
    </form>
	)
  }
}

class DeleteButton extends React.Component {
  render(){
    return (
      <button onClick={this.props.onButtonClick} disabled={this.props.buttonDisabled}>
      	Delete Last Item
      </button>
    )
  }
}

class ItemList extends React.Component {
  render(){
    return (
      <ol className="item-list">
      	{this.props.items}
      </ol>
    )
  }
}

class App extends React.Component {
  state = {
    value: '',
    items: [],
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  addItem = event => {
    event.preventDefault();
    this.setState(oldState => ({
      items: [...oldState.items, this.state.value],
    }));
  };

  deleteLastItem = event => {
    this.setState(prevState => ({ items: this.state.items.slice(0, -1) }));
  };

  inputIsEmpty = () => {
    return this.state.value === '';
  };

  noItemsFound = () => {
    return this.state.items.length === 0;
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactND - Coding Practice</h1>
        </header>
        <h2>Shopping List</h2>
    	<ShoppingListForm 
    		onFormSubmit={this.addItem} 
			inputValue={this.state.value} 
			onInputChange={this.handleChange}
			addButtonDisabled={this.inputIsEmpty()}
		/>
		
		<DeleteButton 
			onButtonClick={this.deleteLastItem} 
			buttonDisabled={this.noItemsFound()} 
		/>

        <p className="items">Items</p>
		<ItemList 
			items={this.state.items.map((item, index) => <li key={index}>{item}</li>)} 
		/>        
      </div>
    );
  }
}

export default App;
